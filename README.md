# Lab12

Usefull links:

[https://redislabs.com/blog/get-sql-like-experience-redis/](https://redislabs.com/blog/get-sql-like-experience-redis/)



Ex 1

```
MULTI
HMSET customers:001 first_name "Jane" last_name "Doe"
HMSET customers:002 first_name "Jahn" last_name "Doe"
HMSET customers:003 first_name "Jane" last_name "Smith"
HMSET customers:004 first_name "Jahn" last_name "Smith"
HMSET customers:005 first_name "Jane" last_name "Jones"
HMSET customers:006 first_name "Jahn" last_name "Jones"
ZADD customers 001 customers:001 002 customers:002 003 customers:003 004 customers:004 005 customers:005 006 customers:006
EXEC

MULTI
HMSET orders:1001 customer_numb 002 order_date "10/10/09" order_total 250.85
HMSET orders:1002 customer_numb 002 order_date "2/21/10" order_total 125.89
HMSET orders:1003 customer_numb 003 order_date "11/15/09" order_total 1567.99
HMSET orders:1004 customer_numb 004 order_date "11/22/09" order_total 180.92
HMSET orders:1005 customer_numb 004 order_date "12/15/09" order_total 565.00
HMSET orders:1006 customer_numb 006 order_date "11/22/09" order_total 25.00
HMSET orders:1007 customer_numb 006 order_date "10/8/09" order_total 85.00
HMSET orders:1008 customer_numb 006 order_date "12/29/09" order_total 109.12
ZADD orders 1001 orders:1001 1002 orders:1002 1003 orders:1003 1004 orders:1004 1005 orders:1005 1006 orders:1006 1007 orders:1007 1008 orders:1008
EXEC
```

Ex 2

```
MULTI
HMSET users:1 login "mymyname" name "My name" followers followers:1 following following:1 posts posts:1
HMSET users:2 login "mymyname" name "My name" followers followers:2 following following:2 posts posts:2
HMSET users:3 login "mymyname" name "My name" followers followers:3 following following:3 posts posts:3
LPUSH followers:1 user:2 user:3
LPUSH followers:2 user:1
LPUSH followers:3 user:1
LPUSH following:1 user:2 user:3
LPUSH following:2 user:1
LPUSH following:3 user:1
HMSET posts:1 title "My best title#1" text "some text"
HMSET posts:2 title "My best title#2" text "more text"
HMSET posts:3 title "My best title#5" text "even more text"

ZADD users 1 users:1 2 users:2 3 users:3
ZADD posts 1 posts:1 2 posts:2 3 posts:3
EXEC
```

